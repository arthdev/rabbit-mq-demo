﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace Publisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "127.0.0.1" };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    string message = GetMessage(args);
                    var body = Encoding.UTF8.GetBytes(message);

                    // Inicio das configuracoes para Publisher Confirms
                    // Secao "Publisher Confirms" em https://www.rabbitmq.com/confirms.html
                    // Codigo baseado em https://rianjs.net/2013/12/publisher-confirms-with-rabbitmq-and-c-sharp

                    // Secoes importantes:
                    // When Will Published Messages Be Confirmed by the Broker?
                    
                    // 1. habilitar confirmacao de publish
                    channel.ConfirmSelect();

                    // 2. callback para quando o publish for confirmado
                    channel.BasicAcks += (obj, ea) => {
                        Console.WriteLine("Publish of {0} confirmed at {1}.", 
                            ea.DeliveryTag, 
                            DateTime.Now.ToString("o"));
                    };

                    channel.BasicPublish(exchange: "",
                                         routingKey: "task_queue",
                                         basicProperties: null,
                                         body: body);

                    // 3. Esperar confirmacao. Caso receba negativa de publish (Nack), lanca excecao.
                    channel.WaitForConfirmsOrDie();
                    Console.WriteLine("Sent {0} at {1}", message, DateTime.Now.ToString("o"));
                }
            }

        }
        public static string GetMessage(string[] args)
        {
            return ((args.Length > 0) ? string.Join(" ", args) : "Hello World!");
        }
    }
}
