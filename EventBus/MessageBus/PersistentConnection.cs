using System;
using System.Net.Sockets;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace MessageBus 
{
    public class PersistentConnection : IDisposable
    {
        private readonly string _host;
        private readonly int _retryCount;
        private readonly IConnectionFactory _connectionFactory;
        IConnection _connection;
        bool _disposed;
        object sync_root = new object();
        public PersistentConnection(string host, int retryCount = 5)
        {
            _host = host ?? throw new ArgumentNullException(nameof(host));
            _retryCount = retryCount;
            _connectionFactory = new ConnectionFactory() { HostName = host };
        }

        public bool IsConnected
        {
            get
            {
                return _connection != null && _connection.IsOpen && !_disposed;
            }
        }

        public IModel CreateModel()
        {
            if (!IsConnected)
            {
                throw new InvalidOperationException("No RabbitMQ connections are available to perform this action");
            }

            return _connection.CreateModel();
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;
            try {
                _connection?.Dispose();
            } catch (Exception ex) {
                Console.Write(ex.ToString());
            }
        }

        public bool TryConnect()
        {
            Console.WriteLine("RabbitMQ Client is trying to connect");
            lock (sync_root)
            {
                var policy = RetryPolicy
                    .Handle<SocketException>()
                    .Or<BrokerUnreachableException>()
                    .WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                    {
                        Console.WriteLine("RabbitMQ Client could not connect after {TimeOut}s ({ExceptionMessage})", $"{time.TotalSeconds:n1}", ex.Message);
                    }
                );

                policy.Execute(() =>
                {
                    _connection = _connectionFactory.CreateConnection();
                });

                if (IsConnected)
                {
                    _connection.ConnectionShutdown += OnConnectionShutdown;
                    _connection.CallbackException += OnCallbackException;
                    _connection.ConnectionBlocked += OnConnectionBlocked;

                    Console.WriteLine("RabbitMQ Client acquired a persistent connection to '{0}' and is subscribed to failure events", 
                                      _connection.Endpoint.HostName);

                    return true;
                }
                else
                {
                    Console.WriteLine("FATAL ERROR: RabbitMQ connections could not be created and opened");

                    return false;
                }
            }
        }

        private void OnConnectionBlocked(object sender, ConnectionBlockedEventArgs e)
        {
            if (_disposed) return;

            Console.WriteLine("A RabbitMQ connection is shutdown. Trying to re-connect...");

            TryConnect();
        }

        void OnCallbackException(object sender, CallbackExceptionEventArgs e)
        {
            if (_disposed) return;

            Console.WriteLine("A RabbitMQ connection throw exception. Trying to re-connect...");

            TryConnect();
        }

        void OnConnectionShutdown(object sender, ShutdownEventArgs reason)
        {
            if (_disposed) return;

            Console.WriteLine("A RabbitMQ connection is on shutdown. Trying to re-connect...");

            TryConnect();
        }
    }
}