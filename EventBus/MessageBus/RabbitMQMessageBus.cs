using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace MessageBus
{
    public class RabbitMQMessageBus : IDisposable
    {
        public const string BROKER_NAME = "online_pizza_exchange";
        public EventHandler<IMessage> OnPublishConfirm { get; set; }
        private readonly PersistentConnection _persistentConnection;
        private readonly int _retryCount;
        private IModel _consumerChannel;
        private string _queueName;
        private readonly IDictionary<Type, List<Action<IMessage>>> _subscriptions;
        public RabbitMQMessageBus(PersistentConnection connection, int retryCount = 5)
        {
            _persistentConnection = connection;
            _retryCount = retryCount;
            _subscriptions = new Dictionary<Type, List<Action<IMessage>>>();

            _queueName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        }
        public void Publish(IMessage message)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var policy = RetryPolicy
                .Handle<BrokerUnreachableException>()
                .Or<SocketException>()
                .WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                {
                    Console.WriteLine("Could not publish message: {MessageId} after {Timeout}s ({ExceptionMessage})", @message, $"{time.TotalSeconds:n1}", ex.Message);
                });

            using (var channel = _persistentConnection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: BROKER_NAME, type: "direct");

                var messageName = MakeRoutingKey(message.GetType());
                var jsonMessage = JsonConvert.SerializeObject(message);
                var body = Encoding.UTF8.GetBytes(jsonMessage);

                policy.Execute(() =>
                {
                    var props = channel.CreateBasicProperties();
                    props.Persistent = true;

                    channel.ConfirmSelect();

                    channel.BasicAcks += (obj, ea) => 
                    {
                        OnPublishConfirm?.Invoke(this, @message);
                        Console.WriteLine("Message ID {0} published.", @message);
                    };

                    channel.BasicPublish(exchange: BROKER_NAME,
                        routingKey: messageName,
                        basicProperties: props,
                        body: body);

                    channel.WaitForConfirmsOrDie();
                });
            }
        }
        public void Subscribe<T>(Action<IMessage> action) where T : IMessage
        {
            var messageType = typeof(T);
            if (!_subscriptions.ContainsKey(messageType))
            {
                _subscriptions.Add(messageType, new List<Action<IMessage>>());
            }
            _subscriptions[messageType].Add(action);

            OnMessageSubscriptionAdded(messageType);
        }
        public void Unsubscribe<T>(Action<IMessage> action) where T : IMessage
        {
            var messageType = typeof(T);
            if (_subscriptions.ContainsKey(messageType))
            {
                _subscriptions[messageType].Remove(action);
            }

            OnMessageSubscriptionRemoved(messageType);
        }
        public void StartConsuming()
        {
            _consumerChannel = CreateCustomerChannel();
        }
        public void Dispose()
        {
            _consumerChannel?.Dispose();
        }
        private void OnMessageSubscriptionAdded(Type messageType)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {
                channel.QueueBind(
                    queue: _queueName,
                    exchange: BROKER_NAME,
                    routingKey: MakeRoutingKey(messageType)
                );
            }
        }
        private void OnMessageSubscriptionRemoved(Type messageType)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {
                if (!_subscriptions.ContainsKey(messageType)) {
                    channel.QueueUnbind(
                        queue: _queueName,
                        exchange: BROKER_NAME,
                        routingKey: MakeRoutingKey(messageType)
                    );
                }

                if (!_subscriptions.Any())
                {
                    _queueName = string.Empty;
                    _consumerChannel.Close();
                }
            }
        }
        private IModel CreateCustomerChannel()
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var channel = _persistentConnection.CreateModel();

            channel.ExchangeDeclare(exchange: BROKER_NAME, type: "direct");

            channel.QueueDeclare(queue: _queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var messageName = ea.RoutingKey;

                try {
                    var rawMessage = Encoding.UTF8.GetString(ea.Body);
                    var messageType = GetMessageType(messageName);
                    var message = JsonConvert.DeserializeObject(rawMessage, messageType);

                    foreach (var action in GetActionsForMessage(messageType))
                    {
                        action.Invoke((IMessage) message);
                    }

                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                } catch (Exception e) {
                    Console.WriteLine("Error when receive message: {0}", e.Message);
                }
            };

            channel.BasicConsume(queue: _queueName,
                                 autoAck: false,
                                 consumer: consumer);

            channel.CallbackException += (sender, ea) =>
            {
                _consumerChannel.Dispose();
                _consumerChannel = CreateCustomerChannel();
            };

            return channel;
        }
        private string MakeRoutingKey(Type messageType)
        {
            return messageType.Name;
        }
        private IEnumerable<Action<IMessage>> GetActionsForMessage(Type messageType)
        {
            if (_subscriptions.ContainsKey(messageType)) {
                return _subscriptions[messageType];
            }
            return Enumerable.Empty<Action<IMessage>>();
        }
        private Type GetMessageType(string messageName)
        {
            return _subscriptions.Keys.SingleOrDefault(k => MakeRoutingKey(k) == messageName);
        }
    }
}
