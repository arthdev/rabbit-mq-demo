using IntegrationEvents.Managers;

namespace IntegrationEvents
{
    public abstract class IntegrationEventHandler<T> where T : IntegrationEvent
    {
        protected readonly IIntegrationEventHandlingManager _handlingManager;
        public IntegrationEventHandler(IIntegrationEventHandlingManager handlingManager)
        {
            _handlingManager = handlingManager;
        }
        public void Handle(T @event)
        {
            if (!_handlingManager.WasHandled(@event.Id, this.GetType()))
            {
                HandleEvent(@event);
                _handlingManager.SaveHandling(@event, this);
            }

        }

        protected abstract void HandleEvent(T @event);
    }
}