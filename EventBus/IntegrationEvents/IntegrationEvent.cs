using System;

namespace IntegrationEvents
{
    public abstract class IntegrationEvent
    {
        public Guid Id { get; set; }
    }
}