using System.Collections.Generic;

namespace IntegrationEvents.Managers
{
    public interface IIntegrationEventManager
    {
        void SaveEvent<T>(T @event) where T : IntegrationEvent;
        IEnumerable<IntegrationEvent> LoadUnpublishEvents();
        void MarkEventAsPublished<T>(T @event) where T : IntegrationEvent;
    }
}