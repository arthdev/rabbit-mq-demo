using System;

namespace IntegrationEvents.Managers
{
    public interface IIntegrationEventHandlingManager
    {
        void SaveHandling<T>(T @event, IntegrationEventHandler<T> handler) where T : IntegrationEvent;
        bool WasHandled(Guid eventId, Type handler);
    }
}