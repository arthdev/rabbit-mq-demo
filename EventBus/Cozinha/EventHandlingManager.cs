using System;
using Dapper;
using IntegrationEvents.Managers;
using Newtonsoft.Json;

namespace Cozinha
{
    public class EventHandlingManager : IIntegrationEventHandlingManager
    {
        private readonly DBConnectionFactory _connectionFactory;
        public EventHandlingManager(DBConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public void SaveHandling<T>(T @event, IntegrationEvents.IntegrationEventHandler<T> handler) where T : IntegrationEvents.IntegrationEvent
        {
            var sql = "INSERT INTO public.\"HandledIntegrationEvents\"(\"Id\", \"EventBody\", \"HandleDate\", \"EventType\") VALUES (@Id, @EventBody, CURRENT_TIMESTAMP, @EventType)";
            var handledEvent = new IntegrationEventHandlingLog(@event.Id,
                                                            JsonConvert.SerializeObject(@event),
                                                            @event.GetType().Name);

            var connection = _connectionFactory.GetOpenConnection();
            connection.Execute(sql, @event);
        }

        public bool WasHandled(Guid eventId, Type handler)
        {
            var sql = "SELECT 1 FROM \"HandledIntegrationEvents\" WHERE \"Id\" = @Id";
            var connection = _connectionFactory.GetOpenConnection();
            var eventHandled = connection.QueryFirstOrDefault<bool?>(
                sql, 
                new { Id = eventId }//@TODO: implementar filtro tambem pelo handler
            );

            return eventHandled.HasValue ? eventHandled.Value : false;
        }
    }
}