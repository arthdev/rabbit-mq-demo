using System;
using IntegrationEvents;
using IntegrationEvents.Managers;

namespace Cozinha
{
    public class PedidoFeitoEventHandler : IntegrationEventHandler<PedidoFeitoEvent>
    {
        public PedidoFeitoEventHandler(IIntegrationEventHandlingManager handlingManager) : base(handlingManager)
        {
        }

        protected override void HandleEvent(PedidoFeitoEvent @event)
        {
            Console.WriteLine("Handler {0} is handling event {1}.", this.GetType().Name, @event.GetType().Name);
        }
    }
}