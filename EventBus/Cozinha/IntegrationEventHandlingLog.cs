using System;

namespace Cozinha
{
    public class IntegrationEventHandlingLog
    {
        public Guid Id { get; private set; }
        public string EventBody { get; private set; }
        public string EventType { get; private set; }
        public DateTime? HandleDate { get; private set; }
        public IntegrationEventHandlingLog(Guid id, string eventBody, string eventType)
        {
            this.Id = id;
            this.EventBody = eventBody;
            this.EventType = eventType;
        }

        public IntegrationEventHandlingLog(){}
    }
}