using System.Threading;
using System.Threading.Tasks;
using IntegrationEvents.Managers;
using MediatR;

namespace Cozinha
{
    public abstract class IntegrationEventHandler<T> : 
        IntegrationEvents.IntegrationEventHandler<T>, 
        INotificationHandler<T>
        where T : IntegrationEvent
    {
        public IntegrationEventHandler(IIntegrationEventHandlingManager handlingManager) : base(handlingManager)
        {
        }

        public Task Handle(T notification, CancellationToken cancellationToken)
        {
            return Task.Run(() => Handle(notification));
        }
    }
}