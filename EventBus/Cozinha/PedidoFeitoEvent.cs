using System;

namespace Cozinha
{
    public class PedidoFeitoEvent : IntegrationEvent
    {
        public PedidoFeitoEvent()
        {
            Id = Guid.NewGuid();
        }
        public string Sabores { get; set; }
    }
}