using MediatR;

namespace Cozinha
{
    public abstract class IntegrationEvent : IntegrationEvents.IntegrationEvent, INotification
    {
    }
}