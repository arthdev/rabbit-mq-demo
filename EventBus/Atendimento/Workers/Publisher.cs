using System;
using System.Linq;
using Microsoft.Extensions.Hosting;
using MessageBus;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using IntegrationEvents.Managers;
using Atendimento.Events;

namespace Atendimento.Workers
{
    public class Publisher : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;

        public Publisher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var messageBus = _serviceProvider.GetService<RabbitMQMessageBus>();

            #region Callback para confirmacao de publicacao dos eventos
            messageBus.OnPublishConfirm += (sender, message) =>
            {
                using (var confirmScope = _serviceProvider.CreateScope())
                {
                    confirmScope.ServiceProvider.GetService<IIntegrationEventManager>().MarkEventAsPublished((IntegrationEvent) message);
                }
            };
            #endregion

            #region Buscar eventos e publicar
                while (!stoppingToken.IsCancellationRequested)
                {
                    using (var findEventsScope = _serviceProvider.CreateScope())
                    {
                        var service = findEventsScope.ServiceProvider.GetService<IIntegrationEventManager>();
                        var eventsToPublish = service.LoadUnpublishEvents();
                        foreach (var @event in eventsToPublish)
                        {
                            messageBus.Publish((IntegrationEvent)@event);                     
                        }

                        Console.WriteLine("{0} integration events were published.", eventsToPublish.Count());
                    }

                    await Task.Delay(TimeSpan.FromSeconds(10));
                }
            #endregion
        }
    }
}