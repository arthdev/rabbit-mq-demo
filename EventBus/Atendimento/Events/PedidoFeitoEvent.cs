using System;

namespace Atendimento.Events
{
    public class PedidoFeitoEvent : IntegrationEvent
    {
        public string Sabores { get; private set; }
        private PedidoFeitoEvent() // Json.NET, ORMs, etc.
        {
        }
        public PedidoFeitoEvent(string sabores) : base()
        {
            Sabores = sabores;
        }
    }
}