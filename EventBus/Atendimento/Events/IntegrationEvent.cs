﻿using MessageBus;
using System;

namespace Atendimento.Events
{
    public abstract class IntegrationEvent : IntegrationEvents.IntegrationEvent, IMessage
    {
        protected IntegrationEvent()
        {
            Id = Guid.NewGuid();
        }
    }
}