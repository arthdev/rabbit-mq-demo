﻿using Atendimento.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Atendimento.Infra;
using MessageBus;
using IntegrationEvents.Managers;
using MediatR;

namespace Atendimento
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mqHost = "localhost";
            services.AddSingleton<PersistentConnection>(ctx => new PersistentConnection(mqHost));
            services.AddSingleton<RabbitMQMessageBus>();

            services.AddHostedService<Workers.Publisher>();

            var connStr = Configuration.GetConnectionString("DB");
            services.AddScoped<DBConnectionFactory>(ctx => new DBConnectionFactory(connStr));
            services.AddScoped<IIntegrationEventManager, IntegrationEventManager>();

            services.AddMediatR(GetType().Assembly);
            
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

           app.UseRouting();
           app.UseEndpoints(endpoints =>
            {   
            endpoints.MapControllers();
            });
        }
    }
}
