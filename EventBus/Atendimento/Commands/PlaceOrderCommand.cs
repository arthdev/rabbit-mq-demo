using System;
using System.Collections.Generic;

namespace Atendimento.Commands
{
    public class PlaceOrderCommand : Command
    {
        public DateTime PlacedAt { get; private set; }
        public int CustomerId { get; private set; }
        public IEnumerable<OrderItemDto> OrderItems { get; private set; }
        public PlaceOrderCommand(DateTime placedAt, IEnumerable<OrderItemDto> orderItems, int customerId)
        {
            PlacedAt = placedAt;
            OrderItems = orderItems;
            CustomerId = customerId; // @@todo controller deve simular que pega da session...   
        }

        public class OrderItemDto
        {
            public string Description { get; set; }
            public decimal Total { get; set; }
        }
    }
}