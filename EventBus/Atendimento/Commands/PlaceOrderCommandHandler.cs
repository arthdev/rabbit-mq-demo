using System.Linq;
using Atendimento.Commands;
using Atendimento.Events;
using IntegrationEvents.Managers;
using MediatR;

namespace Atendimento.Handlers.Commands
{
    public class PlaceOrderCommandHandler : RequestHandler<PlaceOrderCommand>
    {
        private readonly IIntegrationEventManager _manager;

        public PlaceOrderCommandHandler(IIntegrationEventManager manager)
        {
            //@@TODO: dbContext, etc
            _manager = manager;
        }
        protected override void Handle(PlaceOrderCommand command)
        {
            // @TODO: add integration event na fila para publicacao rabbitmq
            var @event = new PedidoFeitoEvent(string.Join(',' , command.OrderItems.Select(o => o.Description)));
            _manager.SaveEvent(@event);
        }
    }
}