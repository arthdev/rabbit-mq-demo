using System;

namespace Atendimento
{
    public class IntegrationEventLog
    {
        public Guid Id { get; set; }
        public string EventBody { get; set; }
        public string EventType { get; set; }
        public bool Published { get; set; }
        public DateTime? PublishDate { get; set; }
    }
}