using System;
using System.Collections.Generic;
using System.Linq;
using Atendimento.Infra;
using Newtonsoft.Json;
using Dapper;
using IntegrationEvents.Managers;
using IntegrationEvents;

namespace Atendimento.Services
{
public class IntegrationEventManager : IIntegrationEventManager
{
    private readonly DBConnectionFactory _connectionFactory;
    public IntegrationEventManager(DBConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }
    public void SaveEvent<T>(T @event) where T : IntegrationEvent
    {
        var sql = "INSERT INTO \"IntegrationEvents\" (\"Id\", \"EventType\", \"EventBody\" ,\"Published\") VALUES (@Id, @EventType, @EventBody, @Published)";
        var connection = _connectionFactory.GetOpenConnection();
        connection.Execute(sql,
                           new IntegrationEventLog()
                           {
                               Id = @event.Id,
                               Published = false,
                               EventType = @event.GetType().Name,
                               EventBody = JsonConvert.SerializeObject(@event)
                           });
    }

    public IEnumerable<IntegrationEvent> LoadUnpublishEvents()
    {
        var sql = "SELECT * FROM \"IntegrationEvents\" WHERE \"Published\" = false";
        var connection = _connectionFactory.GetOpenConnection();
        var eventsLogs = connection.Query<IntegrationEventLog>(sql);

        var eventsToPublish = eventsLogs.Select(el => 
        {
            var eventType = AppDomain.CurrentDomain
                                     .GetAssemblies()
                                     .SelectMany(a => a.GetTypes())
                                     .Where(t => t.Name == el.EventType 
                                        && (typeof(IntegrationEvent).IsAssignableFrom(t)) 
                                        && !t.IsInterface 
                                        && !t.IsAbstract)
                                     .FirstOrDefault();

            var @event = JsonConvert.DeserializeObject(el.EventBody, eventType);
            return (IntegrationEvent) @event;
        });

        return eventsToPublish;
    }

    public void MarkEventAsPublished<T>(T @event) where T : IntegrationEvent
    {
        var connection = _connectionFactory.GetOpenConnection();
        connection.Execute("UPDATE \"IntegrationEvents\" SET \"Published\" = true, \"PublishDate\" = CURRENT_TIMESTAMP WHERE \"Id\" = @Id", @event);
    }
}
}