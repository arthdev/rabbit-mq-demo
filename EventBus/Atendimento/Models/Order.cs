using System;
using System.Collections.Generic;

namespace Atendimento.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime PlacedAt { get; set; }
        public List<OrderItem> Items { get; set; }
    }
}