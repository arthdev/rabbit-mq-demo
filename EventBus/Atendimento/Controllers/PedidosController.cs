using System;
using System.Collections.Generic;
using Atendimento.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Atendimento.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PedidosController(IMediator mediator)
        {
            _mediator = mediator;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] string sabores)
        {
            var userId = 1;

            var orderItems = new List<PlaceOrderCommand.OrderItemDto>() {
                new PlaceOrderCommand.OrderItemDto() {
                    Description = sabores.Split(',')[0],
                    Total = 100.99m
                },
                new PlaceOrderCommand.OrderItemDto() {
                    Description = sabores.Split(',')[1],
                    Total = 200.44m
                }
            };

            var command = new PlaceOrderCommand(
                DateTime.Now,
                orderItems,
                userId
            );

            _mediator.Send(command);

            return Ok(sabores);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }        
    }
}