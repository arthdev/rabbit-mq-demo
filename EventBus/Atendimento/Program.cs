﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Atendimento
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            /* 
            host.Services é o root provider. Tentar resolver servico scoped para ver (lanca excecao dizendo).
            */
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
    }
}
